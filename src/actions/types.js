const ADD_BOOK = "ADD_BOOK";
const UPDATE_BOOK = "UPDATE_BOOK";
const DELETE_BOOK = "DELETE_BOOK";

export { ADD_BOOK, UPDATE_BOOK, DELETE_BOOK };
